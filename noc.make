core = 6.20
api = 2

; Drupal.org contrib projects
projects[context][subdir] = contrib
projects[context][version] = 3.0

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.8

projects[features][subdir] = contrib
projects[features][version] = 1.0

projects[og][subdir] = contrib
projects[og][version] = 2.1

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.5

projects[purl][subdir] = contrib
projects[purl][version] = 1.0-beta13

projects[spaces][subdir] = contrib
projects[spaces][version] = 3.1

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

projects[token][subdir] = contrib
projects[token][version] = 1.15

projects[views][subdir] = contrib
projects[views][version] = 2.12


; Libraries
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-6.x-2.0-beta1.tar.gz"

; NOC projects
projects[noc_community][type] = module
projects[noc_community][download][type] = git
projects[noc_community][download][url] = git://github.com/ceardach/noc_community.git
projects[noc_community][download][branch] = master
projects[noc_community][download][revision] = 93a3254dd

